//
//  TableCell.swift
//  TableExpansion
//
//  Created by Muqeem Ahmad on 10/03/21.
//

import UIKit

class VehicleCell: UITableViewCell {

    var added: Bool = false
    var verticalStackView: UIStackView? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        if added == false {
            added = true
            addSubViewsAndlayout()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Add and sets up subviews with programmically added constraints
    func addSubViewsAndlayout() {
        self.contentView.addSubview(checkImage) //will crash if not added
        self.contentView.addSubview(idLabel) //will crash if not added
        self.contentView.addSubview(makeLabel) //will crash if not added
        self.contentView.addSubview(dropdownImage) //will crash if not added
        self.contentView.addSubview(cellButton) //will crash if not added
        cellButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        checkImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        checkImage.heightAnchor.constraint(equalToConstant: 30).isActive = true
        checkImage.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 10).isActive = true
        checkImage.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        idLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        idLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        idLabel.leftAnchor.constraint(equalTo: checkImage.rightAnchor, constant: 10).isActive = true
        idLabel.widthAnchor.constraint(equalToConstant: 30).isActive = true

        makeLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        makeLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        makeLabel.leftAnchor.constraint(equalTo: idLabel.rightAnchor, constant: 10).isActive = true
        
        dropdownImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        dropdownImage.heightAnchor.constraint(equalToConstant: 25).isActive = true
        dropdownImage.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -20).isActive = true
        dropdownImage.widthAnchor.constraint(equalToConstant: 25).isActive = true

        cellButton.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0).isActive = true
        cellButton.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 0).isActive = true
        cellButton.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: 0).isActive = true
        cellButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    var vehicle : Vehicle? {
        didSet {
//            checkImage.image =  #imageLiteral(resourceName: "checkmark.square")
            self.idLabel.text = String(vehicle!.id)
            self.makeLabel.text = vehicle?.make
//            self.dropButton.setImage(#imageLiteral(resourceName: "dropdown"), for: .normal)

        }
    }
    
    private let checkImage : UIImageView = {
        let imgView = UIImageView()
        imgView.image = UIImage(systemName: "checkmark.square")
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        imgView.tintColor = #colorLiteral(red: 0.3529411765, green: 0.3529411765, blue: 0.3529411765, alpha: 1)
        
        imgView.translatesAutoresizingMaskIntoConstraints = false //Must use
        return imgView
    }()
    
    private let idLabel: UILabel = {
        let label = UILabel()
        label.text = " Title "
        label.textColor = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)

        label.translatesAutoresizingMaskIntoConstraints = false //Must use
        return label
    }()

    private let makeLabel: UILabel = {
        let label = UILabel()
        label.text = " Title "
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 17)
        //…
        label.translatesAutoresizingMaskIntoConstraints = false //Must use
        return label
    }()
    
    private let dropdownImage : UIImageView = {
        let imgView = UIImageView()
        imgView.image = UIImage(systemName: "chevron.right")
        imgView.contentMode = .scaleAspectFit
//        imgView.clipsToBounds = true
        imgView.tintColor = #colorLiteral(red: 0.3529411765, green: 0.3529411765, blue: 0.3529411765, alpha: 1)
        
        imgView.translatesAutoresizingMaskIntoConstraints = false //Must use
        return imgView
        
        
    }()
    
    private let cellButton : UIButton = {
        let btn = UIButton()
        btn.imageView?.contentMode = .scaleAspectFit

        btn.translatesAutoresizingMaskIntoConstraints = false //Must use
        return btn
    }()
    
    @objc func buttonAction(sender: UIButton!) {
        if verticalStackView == nil {
            verticalStackView = addVerticalStackview()
            self.contentView.addSubview(verticalStackView!) //will crash if not added
            checkImage.image = UIImage(systemName: "checkmark.square.fill")
            dropdownImage.image = UIImage(systemName: "chevron.down")
            self.backgroundColor = .darkGray
        } else {
            verticalStackView?.removeFromSuperview()
            verticalStackView = nil
            checkImage.image = UIImage(systemName: "checkmark.square")
            dropdownImage.image = UIImage(systemName: "chevron.right")
            self.backgroundColor = .white
        }
        
        NotificationCenter.default.post(name: Notification.Name("buttonClicked"), object: nil)
    }
    
    func addVerticalStackview() -> UIStackView {

        //Stack View
        let stackView   = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.vertical
        stackView.distribution  = UIStackView.Distribution.equalSpacing
        stackView.alignment = UIStackView.Alignment.center
        stackView.spacing   = 16.0

        stackView.addArrangedSubview(addHorizontalStack(key: "VIN", value: self.vehicle!.vin))
        stackView.addArrangedSubview(addHorizontalStack(key: "Year", value: String(self.vehicle!.year)))
        stackView.addArrangedSubview(addHorizontalStack(key: "Make", value: self.vehicle!.make))
        stackView.addArrangedSubview(addHorizontalStack(key: "Value", value: "$" + String(self.vehicle!.value)))
        stackView.addArrangedSubview(addHorizontalStack(key: "Length", value: String(self.vehicle!.length) + " ft" ))
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(stackView)

        stackView.topAnchor.constraint(equalTo: cellButton.bottomAnchor, constant: 0).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 13).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10).isActive = true
        
        return stackView
    }

    func addHorizontalStack(key: String, value: String) -> UIStackView {
        
        let nameLabel = UILabel()
        nameLabel.widthAnchor.constraint(equalToConstant: 70.0).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.text  = key
        nameLabel.textColor  = #colorLiteral(red: 0.6078431373, green: 0.6078431373, blue: 0.6196078431, alpha: 1)
        nameLabel.textAlignment = .left
        
        let valueLabel = UILabel()
        valueLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        valueLabel.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        valueLabel.text  = value
        valueLabel.textColor  = .black
        valueLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        valueLabel.textAlignment = .left
        
        //Stack View
        let stackView   = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.horizontal
        stackView.distribution  = UIStackView.Distribution.equalSpacing
        stackView.alignment = UIStackView.Alignment.center
        stackView.spacing   = 16.0

        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(valueLabel)
        stackView.translatesAutoresizingMaskIntoConstraints = false

        self.contentView.addSubview(stackView)

//        stackView.topAnchor.constraint(equalTo: dropButton.bottomAnchor, constant: 0).isActive = true
//        stackView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 0).isActive = true
//        stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
        
        return stackView
    }

}
