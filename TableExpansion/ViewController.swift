//
//  ViewController.swift
//  TableExpansion
//
//  Created by Muqeem Ahmad on 10/03/21.
//

import UIKit

class ViewController: UIViewController {
    let cellId = "VehicleCell"
    var vehicleJson: [Dictionary<String, Any>] = []
    var vehicles = [Vehicle]()
    private var myTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
        self.view.addSubview(headerLabel)

        myTableView = UITableView()
        myTableView.register(VehicleCell.self, forCellReuseIdentifier: cellId)
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.backgroundColor = .clear
        myTableView.bounces = false
        myTableView.tableFooterView = UIView() //remove extra empty cells
        self.view.addSubview(myTableView)
        
        headerLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        headerLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        headerLabel.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10).isActive = true
        headerLabel.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        
        myTableView.translatesAutoresizingMaskIntoConstraints = false //Must use
        myTableView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 20).isActive = true
        myTableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        myTableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        myTableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        myTableView.rowHeight = UITableView.automaticDimension
        
        readJson()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.buttonClicked),
            name: NSNotification.Name(rawValue: "buttonClicked"),
            object: nil)
    }

    @objc private func buttonClicked(notification: NSNotification){
        //do stuff using the userInfo property of the notification object
        
        myTableView.reloadData()
    }
    
    private let headerLabel: UILabel = {
        let label = UILabel()
        label.text = "Choose Equipment"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 35)

        label.translatesAutoresizingMaskIntoConstraints = false //Must use
        return label
    }()
    
    private func readJson() {
        do {
            if let file = Bundle.main.url(forResource: "assignment", withExtension: "json") {
                let data = try Data(contentsOf: file)
                do {
                    let decoder = JSONDecoder()
                    let jsonVehicles = try decoder.decode(Array<Vehicle>.self, from: data)
                    vehicles = jsonVehicles
                    myTableView.reloadData()
                } catch {
                    print(error)
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vehicles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath as IndexPath) as! VehicleCell
        cell.backgroundColor = .white
        cell.selectionStyle = .none
        cell.vehicle = vehicles[indexPath.row]
        return cell
    }
    
}
