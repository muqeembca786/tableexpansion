//
//  Vehicle.swift
//  TableExpansion
//
//  Created by Muqeem Ahmad on 10/03/21.
//

import Foundation

struct Vehicle : Codable {
    let id: Int
    let vin: String
    let year: Int
    let make: String
    let value: Double
    let length: Double
    
}
